main();

function main() {
    const canvas = document.querySelector("#GLcanvas");
    const gl = canvas.getContext('webgl'); // Initialize the WebGL context

    if (gl == null) { // Only continue if WebGL is working
        alert('WebGL is not working. It may not be supported by your browser or device.')
        return;
    }
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // set color to black, fully opaque.
    gl.clear(gl.COLOR_BUFFER_BIT); // Clear the color buffer with the color specified in the line above
    
}