#WebGL Steps

##Main() function
1. Initialize the gl context
2. Check if WebGL is working, or if it isn't supported by the browser
3. Set the clear color and clear the canvas fully
4. Declare and store the the programs for the vertex and fragment shader
5. Initialize the shader programs
6. Initialize the buffers
7. Draw the scene